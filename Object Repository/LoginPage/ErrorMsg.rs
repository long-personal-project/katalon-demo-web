<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ErrorMsg</name>
   <tag></tag>
   <elementGuidId>f9e8222b-11bb-4a19-bb3d-b47c6ea86744</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'error-message-container error']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>error-message-container error</value>
      <webElementGuid>3894f871-7761-4319-a30f-5ec3b57801f6</webElementGuid>
   </webElementProperties>
</WebElementEntity>

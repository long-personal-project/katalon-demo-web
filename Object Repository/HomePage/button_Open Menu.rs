<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Open Menu</name>
   <tag></tag>
   <elementGuidId>e1898b98-612b-462b-b5c4-a25e248a2e43</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#react-burger-menu-btn</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='react-burger-menu-btn']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>b33d6535-26c5-4de0-9077-0b35481e1e22</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>react-burger-menu-btn</value>
      <webElementGuid>721261fe-bacd-4a4f-bb5c-d10046dbb162</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Open Menu</value>
      <webElementGuid>63af32f0-ea13-4e50-aaca-e7595321182d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;react-burger-menu-btn&quot;)</value>
      <webElementGuid>25dd2dc7-cfb8-4962-ab1f-87281c6aabf0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='react-burger-menu-btn']</value>
      <webElementGuid>8e9ff280-c5a6-42f9-aba3-a2dafcc45ad3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='menu_button_container']/div/div/div/button</value>
      <webElementGuid>efc1e438-e754-4025-9c23-07cb9adf983f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='About'])[1]/preceding::button[1]</value>
      <webElementGuid>e75d7131-d47c-4b94-8bd1-654862275977</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Close Menu'])[1]/preceding::button[1]</value>
      <webElementGuid>78d40ef0-f15a-4f67-bfbf-e0a24a4b3bd8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Open Menu']/parent::*</value>
      <webElementGuid>828873c9-b6b8-4d95-89f8-ee4c980ad28d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button</value>
      <webElementGuid>22d61d41-f581-452a-b4df-597d8e83f2af</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'react-burger-menu-btn' and (text() = 'Open Menu' or . = 'Open Menu')]</value>
      <webElementGuid>07f2a39e-5430-463f-94e4-4561f499c877</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
